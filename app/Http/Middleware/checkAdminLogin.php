<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*If user already login*/
        if (Auth::check())
        {
            $user = Auth::user();
            /*If level = 1 (Admin) and status = 1 (Active), passed*/
            if ($user->level == 1 && $user->status == 1)
            {
                return $next($request);
            }
            else
            {
                Auth::logout();
                return redirect()->route('getLogin');
            }
        }
        else
        {
            return redirect('admin/login');
        }
    }
}
