<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Article;
use App\Category;

class ArticleController extends Controller
{
    public function show($id)
    {
    	// $article = Article::findOrFail($id);
        $article = Article::where('slug', $id)->first();
        $categories = Category::orderBy('name', 'asc')->get();
        $this->data['article'] = $article;
        $this->data['categories'] = $categories;
    	return view('articles.show', $this->data);
    }

    public function search(Request $request)
    {
        $searchKey = Input::get('key');
    	$articles = Article::where('title', 'LIKE', '%' . $searchKey . '%')->orderBy('created_at', 'desc')->paginate(5);
        $categories = Category::orderBy('name', 'asc')->get();
        $this->data['articles'] = $articles;
        $this->data['categories'] = $categories;
        $this->data['searchKey'] = $searchKey;
    	return view('articles.results', $this->data);
    }
}
