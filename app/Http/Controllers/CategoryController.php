<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use App\Category;

class CategoryController extends Controller
{
    public function show($categoryName)
    {
    	$articles = Article::where('category', $categoryName)->orderBy('created_at', 'desc')->paginate(5);
    	$categories = Category::orderBy('name', 'asc')->get();
    	$this->data['articles'] = $articles;
    	$this->data['categories'] = $categories;
    	$this->data['cateName'] = $categoryName;
    	return view('categories.show', $this->data);
    }
}
