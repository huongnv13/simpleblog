<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use App\Category;

class HomeController extends Controller
{
    public function index()
    {
    	$articles = Article::orderBy('created_at', 'desc')->paginate(5);
    	$categories = Category::orderBy('name', 'asc')->get();
    	$this->data['articles'] = $articles;
    	$this->data['categories'] = $categories;
    	return view('index', $this->data);
    }
}
