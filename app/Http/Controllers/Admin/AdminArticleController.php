<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use App\Article;
use App\Category;

class AdminArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'List articles';
        $listArt = Article::orderBy('created_at', 'desc')->paginate(5);
        $this->data['listArt'] = $listArt;
        return view('admin.articles.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Add Article';
        $listCate = Category::all();
        $this->data['listCate'] = $listCate;
        return view('admin.articles.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'txtTitle' => 'required',
            'txtCategory' => 'required',
            'txtContent' => 'required',
            'txtAuthor' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails())
        {
            return Redirect::to('admin/article/create')->withErrors($validator);
        }
        else
        {
            $article = new Article;
            $article->title = Input::get('txtTitle');
            $article->category = Input::get('txtCategory');
            $article->sort_content = Input::get('txtSortContent');
            $article->content = Input::get('txtContent');
            $article->slug = Input::get('txtSlug');
            $article->author = Input::get('txtAuthor');
            if ($article->save())
            {
                Session::flash('message', 'Successfully created article');
                return Redirect::to('admin/article');
            }
            else
            {
                Session::flash('message', 'Unsuccessfully created article');
                return Redirect::to('admin/article/create');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $this->data['title'] = 'Edit Article';
        $this->data['article'] = $article;
        $listCate = Category::all();
        $this->data['listCate'] = $listCate;
        return view('admin.articles.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'txtTitle' => 'required',
            'txtCategory' => 'required',
            'txtContent' => 'required',
            'txtAuthor' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails())
        {
            return Redirect::to('admin/article/' . $id . '/edit')->withErrors($validator);
        }
        else
        {
            $article = Article::find($id);
            $article->title = Input::get('txtTitle');
            $article->category = Input::get('txtCategory');
            $article->sort_content = Input::get('txtSortContent');
            $article->content = Input::get('txtContent');
            $article->slug = Input::get('txtSlug');
            $article->author = Input::get('txtAuthor');
            if ($article->save())
            {
                Session::flash('message', 'Successfully updated article');
                return Redirect::to('admin/article');
            }
            else
            {
                Session::flash('message', 'Unsuccessfully updated article');
                return Redirect::to('admin/article/' . $id . '/edit');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        if ($article->delete())
        {
            Session::flash('message', 'Successfully deleted article');
            return Redirect::to('admin/article');
        }
        else
        {
            Session::flash('message', 'Unsuccessfully deleted article');
            return Redirect::to('admin/article');
        }
    }
}
