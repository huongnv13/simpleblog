<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\LoginRequest;
use Auth;
use App\User;

class AdminLoginController extends Controller
{

    public function getLogin()
    {
    	if (Auth::check())
    	{
    		return redirect('admin');
    	}
    	else
    	{
			return view('admin.login');
    	}
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function postLogin(LoginRequest $request)
    {
    	$login = [
            'email' => $request->txtEmail,
            'password' => $request->txtPassword,
            'level' => 1,
            'status' => 1
        ];
        if (Auth::attempt($login))
        {
        	return redirect('admin');
        }
        else
        {
        	return redirect()->back()->with('status', 'Email or Password is not correct');
        }
    }

    /**
     * action admincp/logout
     * @return RedirectResponse
     */
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }
}
