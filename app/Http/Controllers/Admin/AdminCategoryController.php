<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use App\Category;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'List category';
        $listCate = Category::orderBy('created_at', 'desc')->paginate(5);
        $this->data['listCate'] = $listCate;
        return view('admin.categories.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Add Category';
        return view('admin.categories.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'txtName' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails())
        {
            return Redirect::to('admin/category/create')->withErrors($validator);
        }
        else
        {
            $category = new Category;
            $category->name = Input::get('txtName');
            $category->slug = Input::get('txtSlug');
            $category->desc = Input::get('txtDesc');
            if ($category->save())
            {
                Session::flash('message', 'Successfully created category');
                return Redirect::to('admin/category');
            }
            else
            {
                Session::flash('message', 'Unsuccessfully created category');
                return Redirect::to('admin/category/create');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $this->data['title'] = 'Edit Category';
        $this->data['category'] = $category;
        return view('admin.categories.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'txtName' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails())
        {
            return Redirect::to('admin/category/' . $id . '/edit')->withErrors($validator);
        }
        else
        {
            $category = Category::find($id);
            $category->name = Input::get('txtName');
            $category->slug = Input::get('txtSlug');
            $category->desc = Input::get('txtDesc');
            if ($category->save())
            {
                Session::flash('message', 'Successfully updated category');
                return Redirect::to('admin/category');
            }
            else
            {
                Session::flash('message', 'Unsuccessfully updated category');
                return Redirect::to('admin/category/' . $id . '/edit');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if ($category->delete())
        {
            Session::flash('message', 'Successfully deleted category');
            return Redirect::to('admin/category');
        }
        else
        {
            Session::flash('message', 'Unsuccessfully deleted category');
            return Redirect::to('admin/category');
        }
    }
}
