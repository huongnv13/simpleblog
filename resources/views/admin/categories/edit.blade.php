@extends("layout.adminLayout")
@section("title", $title)
@section("content")
<h1>Edit Category</h1>
<form action="{{ url('admin/category') }}/{{ $category->id }}" method="POST">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}
  @if(count($errors) >0)
  <ul>
   @foreach($errors->all() as $error)
   <li class="text-danger">{{ $error }}</li>
   @endforeach
 </ul>
 @endif
 <div class="box">
   <div class="box-body row">
     <div class="form-group col-md-12">
       <label>Name</label>
       <input type="text" name="txtName" class="form-control" value="{{ $category->name }}">
     </div>
     <div class="form-group col-md-12">
       <label>Slug</label>
       <input type="text" name="txtSlug" class="form-control"  value="{{ $category->slug }}">
     </div>
     <div class="form-group col-md-12">
       <label>Desc</label>
       <textarea name="txtDesc" class="form-control">{{ $category->desc }}</textarea>
     </div>
   </div>
   <div class="box-footer row">
     <button type="submit" class="btn btn-success">
       <i class="fa fa-save"></i>
       <span>Save and back</span>
     </button>
   </div>
 </div>
</form>
@endsection

@section("footer-script")
<script>
  $( document ).ready(function() {
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination span').addClass('page-link');
  });
</script>
@endsection