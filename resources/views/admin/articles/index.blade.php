@extends("layout.adminLayout")
@section("title", $title)
@section("content")
<h1>Articles</h1>
<a href="{{ url('admin/article/create') }}" class="btn btn-success">
    <i class="fa fa-plus"></i>
    <span>Add Article</span>
</a>
<p style="height: 5px"></p>
@if (Session::has('message'))
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="box">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-sm-12">
                <table id="myTable" class="table table-bordered table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting">Title</th>
                            <th class="sorting">Category</th>
                            <th class="sorting">Author</th>
                            <th class="sorting">Actions</th></tr>
                        </thead>
                        <tbody>
                            @if (isset($listArt) && count($listArt) >0)
                            @foreach($listArt as $art)
                            <tr role="row" class="odd">
                                <td>{{ $art->title }}</td>
                                <td>{{ $art->category }}</td>
                                <td>{{ $art->author }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ url('admin/article')}}/{{ $art->id }}/edit" class="btn btn-info">
                                            <i class="fa fa-edit"></i>
                                            <span>Edits</span>
                                        </a>
                                        <a href="#" class="btn btn-danger btnDelete" data-value="{{ $art->id }}">
                                            <i class="fa fa-edit"></i>
                                            <span>Delete</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div style="float:right">
                      <!-- Pagination -->
                      <ul class="pagination justify-content-center mb-4">
                          {{ $listArt->render() }}
                      </ul>
                  </div>
              </div>
          </div>
      </div>

  </div>
  <form action="" method="post" id="formDelete">
      <input type="hidden" name="_method" value="DELETE">
      {{ csrf_field() }}
  </form>
  <div id="confirm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm delete</h4>
            </div>
            <div class="modal-body">
                <p> Are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section("footer-script")
<script>
    $( document ).ready(function() {
      $('.pagination li').addClass('page-item');
      $('.pagination li a').addClass('page-link');
      $('.pagination span').addClass('page-link');

      $('.btnDelete').click(function() {
        var userId = $(this).attr('data-value');
        $('#confirm')
        .modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function (e) {
            var actionLink = "{{ url('admin/article')}}/"+ userId;
            $('#formDelete').attr('action', actionLink).submit();
        });
    });
  });
</script>
@endsection