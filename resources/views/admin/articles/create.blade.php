@extends("layout.adminLayout")
@section("title", $title)
@section("content")
<h1>Add Article</h1>
<form action="{{ url('admin/article') }}" method="POST">
 {{ csrf_field() }}
 @if(count($errors) >0)
 <ul>
   @foreach($errors->all() as $error)
   <li class="text-danger">{{ $error }}</li>
   @endforeach
 </ul>
 @endif
 <div class="box">
   <div class="box-body row">
     <div class="form-group col-md-12">
       <label>Title</label>
       <input type="text" name="txtTitle" class="form-control" value="{{ old('txtTitle') }}">
     </div>
     <div class="form-group col-md-12">
       <label>Category</label>
       <select class="form-control" name="txtCategory"  value="{{ old('txtCategory') }}">
         @foreach($listCate as $cate)
         <option value="{{ $cate->id }}">{{ $cate->name }}</option>
         @endforeach
       </select>
     </div>
     <div class="form-group col-md-12">
       <label>Sort Description</label>
       <textarea id="txtSortContent" name="txtSortContent" class="form-control">{{ old('txtSortContent') }}</textarea>
     </div>
     <div class="form-group col-md-12">
       <label>Content</label>
       <textarea id="txtContent" name="txtContent" class="form-control">{{ old('txtContent') }}</textarea>
     </div>
     <div class="form-group col-md-12">
       <label>Slug</label>
       <input type="text" name="txtSlug" class="form-control"  value="{{ old('txtSlug') }}">
     </div>
     <input type="text" name="txtAuthor" class="form-control" value="{{ Auth::user()->id }}" hidden>
   </div>
   <div class="box-footer row">
     <button type="submit" class="btn btn-success">
       <i class="fa fa-save"></i>
       <span>Save and back</span>
     </button>
   </div>
 </div>
</form>
@endsection

@section("footer-script")
<script>
  $( document ).ready(function() {
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination span').addClass('page-link');

    CKEDITOR.replace('txtContent');
    CKEDITOR.replace('txtSortContent');
  });
</script>
@endsection