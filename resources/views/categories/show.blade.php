@extends("layout.layout")
@section("title", $cateName)
@section("content")
    <!-- Navigation -->
    @include("layout.navigation")

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Category
            <small>{{ $cateName }}</small>
          </h1>
          
          @foreach($articles as $article)
            <!-- Blog Post -->
            <div class="card mb-4">
              {{-- <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap"> --}}
              <div class="card-body">
                <h2 class="card-title">{{ $article->title }}</h2>
                <p class="card-text">{!! $article->sort_content !!}</p>
                <a href="{{ url('article', $article->slug) }}" class="btn btn-primary">Read More &rarr;</a>
              </div>
              <div class="card-footer text-muted">
                Posted on {{ $article->created_at }} by
                <b>{{ $article->author }}</b>
              </div>
            </div>
          @endforeach

          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            {{ $articles->render() }}
          </ul>

        </div>

        <!-- Sidebar Widgets Column -->
        @include("layout.sidebar")

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection

@section("footer-script")
  <script>
    $( document ).ready(function() {
      $('.pagination li').addClass('page-item');
      $('.pagination li a').addClass('page-link');
      $('.pagination span').addClass('page-link');
    });
  </script>
@endsection