@extends("layout.layout")
@section("title", $article->title)
@section("content")
    @include("layout.navigation")
    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">
            {{ $article->title }}
          </h1>
          
            <!-- Blog Post -->
            <div class="card mb-4">
              {{-- <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap"> --}}
              <div class="card-body">
                <p class="card-text">{!! $article->content !!}</p>
              </div>
              <div class="card-footer text-muted">
                Posted on {{ $article->created_at }} by
                <b>{{ $article->author }}</b>
              </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        @include("layout.sidebar")

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection

@section("footer-script")
  <script>
    $( document ).ready(function() {
      $('.pagination li').addClass('page-item');
      $('.pagination li a').addClass('page-link');
      $('.pagination span').addClass('page-link');
    });
  </script>
@endsection