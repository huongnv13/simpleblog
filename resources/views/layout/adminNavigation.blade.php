<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">Administrator</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="nav navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{ url('admin') }}">Home
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('admin/category') }}">Category</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('admin/article') }}">Article</a>
        </li>
      </ul>
    </div>
    <div class="nav navbar-nav navbar-right">
      <a class="nav-link">
        @if (Auth::check())
        Welcome, <strong> {{Auth::user()->name}} </strong>
        @endif
      </a>
      <a href="{{ route('getLogout') }}" class="navbar-brand">Logout</a>
    </div>
  </div>
</nav>