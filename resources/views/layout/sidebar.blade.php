<!-- Sidebar Widgets Column -->
<div class="col-md-4">

  <!-- Search Widget -->
  {!! Form::open(['url' => 'search', 'method' => 'get']) !!}
  <div class="card my-4">
    <h5 class="card-header">Search</h5>
    <div class="card-body">
      <div class="input-group">
        {!! Form::text('key', null, ['class' => 'form-control', 'placeholder' => 'Search for...']) !!}
        <span class="input-group-btn">
          {!! Form::submit('Go', ['class' => 'btn btn-secondary']) !!}
        </span>
      </div>
    </div>
  </div>
  {!! Form::close() !!}

  <!-- Categories Widget -->
  <div class="card my-4">
    <h5 class="card-header">Categories</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            @foreach($categories as $index => $category)
            @if ($index % 2 == 0)
            <li><a href="{{ url('category', $category->name) }}">{{ $category->name }}</a></li>
            @endif
            @endforeach
          </ul>
        </div>
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">
            @foreach($categories as $index => $category)
            @if ($index % 2 != 0)
            <li><a href="{{ url('category', $category->name) }}">{{ $category->name }}</a></li>
            @endif
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- Side Widget -->
          {{-- <div class="card my-4">
            <h5 class="card-header">Side Widget</h5>
            <div class="card-body">
              You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
            </div>
          </div> --}}

        </div>