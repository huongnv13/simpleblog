@include("layout.header")
<body>
    <div class="container">
        @yield("content")
    </div>
</body>
@include("layout.footer")