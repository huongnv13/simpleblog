@include("layout.adminHeader")
<body>
	@include("layout.adminNavigation")
    <div class="container">
        @yield("content")
    </div>
</body>
@include("layout.adminFooter")