<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Home Route*/
Route::get('/', 'HomeController@index');
Route::get('article/{id}', 'ArticleController@show');
Route::get('category/{id}', 'CategoryController@show');
Route::get('search', 'ArticleController@search');

/*Admin Route*/
Route::get('admin/login', ['as' => 'getLogin', 'uses' => 'Admin\AdminLoginController@getLogin']);
Route::post('admin/login', ['as' => 'postLogin', 'uses' => 'Admin\AdminLoginController@postLogin']);
Route::get('admin/logout', ['as' => 'getLogout', 'uses' => 'Admin\AdminLoginController@getLogout']);
Route::resource('admin/category', 'Admin\AdminCategoryController');
Route::resource('admin/article', 'Admin\AdminArticleController');

Route::group(['middleware' => 'checkAdminLogin', 'prefix' => 'admin', 'namespace' => 'Admin'], function() {
    Route::get('/', function() {
        return view('admin.home');
    });
});