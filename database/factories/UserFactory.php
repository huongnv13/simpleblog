<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'status' => 1,
        'level' => 0
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'desc' => $faker->paragraph,
        'slug' => $faker->sentence
    ];
});

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'category' => function () {
            return factory('App\Category')->create()->id;
        },
        'sort_content' => $faker->paragraph,
        'content' => $faker->paragraph,
        'slug' => $faker->sentence,
        'author' => function () {
            return factory('App\User')->create()->id;
        }
    ];
});