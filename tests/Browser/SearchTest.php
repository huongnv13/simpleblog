<?php

namespace Tests\Browser;

use App\Article;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SearchTest extends DuskTestCase
{
    use DatabaseMigrations;
    public function setUp()
    {
        parent::setUp();
        $this->article = factory(Article::class)->create();
    }

    /**
     * User can search Article
     *
     * @return void
     */
    public function testUserCanSearchArticle()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Blog')
                    ->type('key', $this->article->title)
                    ->keys('.btn-secondary', ['{ENTER}'])
                    ->waitForText('Search for ' . $this->article->title)
                    ->assertSee($this->article->title)
                    ->assertSee($this->article->sort_content);
        });
    }
}
