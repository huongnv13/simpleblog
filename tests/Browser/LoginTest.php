<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\User;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * Admin user can login successful
     *
     * @return void
     */
    public function testAdminCanLogin()
    {
        $user = factory(User::class)->create([
            'status' => 1,
            'level' => 1
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('admin/login')
                    ->type('txtEmail', $user->email)
                    ->type('txtPassword', 'secret')
                    ->press('Login')
                    ->assertPathIs('/admin')
                    ->assertSee($user->name);
        });
    }

    /**
     * Non-Admin user cannot login successful
     *
     * @return void
     */
    public function testNonAdminCanNotLogin()
    {
        $user = factory(User::class)->create([
            'status' => 1,
            'level' => 0
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('admin/login')
                    ->type('txtEmail', $user->email)
                    ->type('txtPassword', 'secret')
                    ->press('Login')
                    ->assertPathIs('/admin')
                    ->assertSee('Email or Password is not correct');
        });
    }
}
