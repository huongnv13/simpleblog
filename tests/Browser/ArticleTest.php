<?php

namespace Tests\Browser;

use App\Article;
use App\Category;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

class ArticleTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->article = factory(Article::class)->create();
        $this->category = factory(Category::class)->create();
        $this->user = factory(User::class)->create([
            'status' => 1,
            'level' => 1
        ]);
    }

    /**
     * Admin can view all Category
     *
     * @return void
     */
    public function testAdminCanViewAllArticles()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('admin/article')
                ->assertSee($this->article->title);
        });
    }

    /**
     * Admin can delete Category
     *
     * @return void
     */
    public function testAdminCanDeleteArticle()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('admin/article')
                ->assertSee($this->article->title)
                ->clickLink('Delete')
                ->waitForText('Confirm delete')
                ->click('#delete')
                ->waitForText('Successfully deleted article')
                ->assertDontSee($this->article->title);
        });
    }

    /**
     * Admin can edit Category
     *
     * @return void
     */
    public function testAdminCanEditArticle()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('admin/article')
                ->assertSee($this->article->title)
                ->clickLink('Edits')
                ->waitForText('Edit Article')
                ->type('txtTitle', $this->article->title . 'Laravel')
                ->press('Save and back')
                ->waitForText('Successfully updated article')
                ->assertSee($this->article->title . 'Laravel');
        });
    }

    /**
     * Admin can add new Category
     *
     * @return void
     */
    public function testAdminCanAddArticle()
    {
        $faker = Faker::create('en_US');
        $this->browse(function (Browser $browser) use ($faker) {
            $artTitle = $faker->sentence;
            $artSortDesc = $faker->paragraph;
            $artContent = $faker->paragraph;
            $artSlug = $faker->sentence;
            $browser->loginAs($this->user->id)
                ->visit('admin/article')
                ->assertSee($this->article->title)
                ->clickLink('Add Article')
                ->waitForText('Add Article')
                ->type('txtTitle', $artTitle)
                ->select('txtCategory', $this->category->id)
                ->type('txtSortContent', $artSortDesc)
                ->type('txtContent', $artContent)
                ->type('txtSlug', $artSlug)
                ->type('txtAuthor', $this->user->id)
                ->press('Save and back')
                ->waitForText('Successfully created article')
                ->assertSee($artTitle);
        });
    }
}
