<?php

namespace Tests\Browser;

use App\Category;
use App\User;
use Faker\Factory as Faker;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoryTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->category = factory(Category::class)->create();
        $this->user = factory(User::class)->create([
            'status' => 1,
            'level' => 1
        ]);
    }

    /**
     * Admin can view all Category
     *
     * @return void
     */
    public function testAdminCanViewAllCategories()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                    ->visit('admin/category')
                    ->assertSee($this->category->name);
        });
    }

    /**
     * Admin can delete Category
     *
     * @return void
     */
    public function testAdminCanDeleteCategory()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('admin/category')
                ->assertSee($this->category->name)
                ->clickLink('Delete')
                ->waitForText('Confirm delete')
                ->click('#delete')
                ->waitForText('Successfully deleted category')
                ->assertDontSee($this->category->name);
        });
    }

    /**
     * Admin can edit Category
     *
     * @return void
     */
    public function testAdminCanEditCategory()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('admin/category')
                ->assertSee($this->category->name)
                ->clickLink('Edits')
                ->waitForText('Edit Category')
                ->type('txtName', $this->category->name . 'Laravel')
                ->press('Save and back')
                ->waitForText('Successfully updated category')
                ->assertSee($this->category->name . 'Laravel');
        });
    }

    /**
     * Admin can add new Category
     *
     * @return void
     */
    public function testAdminCanAddCategory()
    {
        $faker = Faker::create('en_US');
        $this->browse(function (Browser $browser) use ($faker) {
            $cateTitle = $faker->sentence;
            $cateSlug = $faker->sentence;
            $cateDesc = $faker->paragraph;
            $browser->loginAs($this->user->id)
                ->visit('admin/category')
                ->assertSee($this->category->name)
                ->clickLink('Add Category')
                ->waitForText('Add Category')
                ->type('txtName', $cateTitle)
                ->type('txtSlug', $cateSlug)
                ->type('txtDesc', $cateDesc)
                ->press('Save and back')
                ->waitForText('Successfully created category')
                ->assertSee($cateTitle);
        });
    }
}
