<?php

namespace Tests\Feature;

use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoriesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->category = factory(Category::class)->create();
        $this->user = factory('App\User')->create([
            'status' => 1,
            'level' => 1
        ]);
    }

    /**
     * User can see all Categories
     *
     * @return void
     */
    public function testUserCanSeeAllCategories()
    {
        $this->get('/')->assertSee($this->category->name);
    }
}
