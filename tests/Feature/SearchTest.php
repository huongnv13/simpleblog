<?php

namespace Tests\Feature;

use App\Article;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SearchTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->article = factory(Article::class)->create();
    }

    /**
     * User can search Article
     *
     * @return void
     */
    public function testUserCanSearchArticle()
    {
        $this->get('/search?key=' . $this->article->title)
                    ->assertStatus(200)
                    ->assertSee($this->article->title);
    }

}