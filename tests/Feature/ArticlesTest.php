<?php

namespace Tests\Feature;

use App\Article;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->article = factory(Article::class)->create();
    }

    /**
     * User can access to Home Page
     *
     * @return void
     */
    public function testUserViewAccessHomePage()
    {
        $this->get('/')->assertStatus(200);
    }

    /**
     * User can view all Articles Title
     *
     * @return void
     */
    public function testUserViewAllArticlesTitle()
    {
        $this->get('/')->assertSee($this->article->title);
    }

    /**
     * User can view all Articles Soft Desc
     *
     * @return void
     */
    public function testUserViewAllArticlesSoftDesc()
    {
        $this->get('/')->assertSee($this->article->sort_content);
    }

    /**
     * User can view all Articles Author
     *
     * @return void
     */
    public function testUserViewAllArticlesAuthor()
    {
        $this->get('/')->assertSee($this->article->owner->name);
    }
}
