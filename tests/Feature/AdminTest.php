<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'status' => 1,
            'level' => 1
        ]);
        $this->category = factory(Category::class)->create();
        $this->article = factory(Article::class)->create();
    }

    /**
     * User will see Login Page if not login yet
     *
     * @return void
     */
    public function testUserSeeLoginPage()
    {
        $this->get('/admin')
            ->assertRedirect('/admin/login');
    }

    /**
     * Admin User can Login
     *
     * @return void
     */
    public function testAdminUserCanLogin()
    {
        $this->actingAs($this->user)
            ->get('/admin')
            ->assertSee($this->user->name);
    }

    /**
     * Admin User can see Category
     *
     * @return void
     */
    public function testAdminUserCanSeeCategories()
    {
        $this->actingAs($this->user)
            ->get('/admin/category')
            ->assertSee($this->category->name);
    }

    /**
     * Admin User can see Article
     *
     * @return void
     */
    public function testAdminUserCanSeeArticles()
    {
        $this->actingAs($this->user)
            ->get('/admin/article')
            ->assertSee($this->article->title);
    }
}
